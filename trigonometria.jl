# MAC0110 - MiniEP7
# Gabriel P S Piedade - 8927356


# Funcoes da parte 1

## Essa primeira função calcula o fatorial de um numero, eu uso ela dentro das 
## das funções de seno, cosseno e tangente

function fac(num)
    if num == 0
        return 1
    end
    if num == 1
        return 1
    else
        return num*fac(num-1) 
    end
end
    

function taylor_sin(x)
    y = 0
    for i = 1:15
        n = 2i - 1
        y += (BigFloat(((-1)^(i+1))*BigFloat(x^n)/BigFloat(fac(n))))     
    end
    return (round.(Float64(y),digits = 3))
end


function taylor_cos(x)
    y = 0
    for i = 0:15
        n = 2i
        y += (BigFloat(((-1)^(i))*BigFloat(x^n)/BigFloat(fac(n))))   
    end
    return (round.(Float64(y),digits = 3))
end

## A minha função tangente não vai até pi, ela vai só até pi/2
function taylor_tan(x)
    y = 0
    function bern(n)
        n *= 2
        A = Vector{Rational{BigInt}}(undef, n + 1)
        for m = 0 : n
                A[m + 1] = 1 // (m + 1)
                for j = m : -1 : 1
                    A[j] = j * (A[j] - A[j + 1])
                end
            end
            return abs(A[1])
        end
    if 0 <= x < pi/2
        
        for i = 1:10
            y += (BigInt(2^2i)*(BigInt(2^2i)-1)*BigFloat(bern(i))*BigFloat(x^(2i-1)))/BigInt(fac(2i))
        end
        return  (round.(Float64(y),digits = 3))
    else
        return "Erro: a função só vai até x < pi/2"
    end       
end

# Funcoes da parte 2

function check_sin(x)
    if taylor_sin(x) == round.(Float64(sind(rad2deg(x))),digits = 3)
        return true
    else
        return false
    end  
end

function check_cos(x)
    if taylor_cos(x) == round.(Float64(cosd(rad2deg(x))),digits = 3)
        return true
    else
        return false
    end
end

function check_tan(x)
    if taylor_tan(x) == round.(Float64(tand(rad2deg(x))),digits = 3)
        return true
    else
        return false 
    end
end

# Funcao da parte 3

using Test
function test()
    
    # seno
    @test taylor_sin(0) == 0
    @test taylor_sin(pi/6) == 0.5
    @test taylor_sin(pi/3) == 0.866
    @test taylor_sin(pi/2) == 1.0
    @test taylor_sin(pi) == 0
    
    #cosseno
    @test taylor_cos(0) == 1
    @test taylor_cos(pi/6) == 0.866
    @test taylor_os(pi/3) == 0.5
    @test taylor_cos(pi/2) == 0
    @test taylor_cos(pi) == -1
    
    #tangente 
    @test taylor_tan(0) == 0
    @test taylor_tan(pi/6) == 0.577
    @test taylor_tan(pi/3) == 1.732
    
    
end

